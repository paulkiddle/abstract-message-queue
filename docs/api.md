<a name="module_abstract-message-queue"></a>

## abstract-message-queue
MessageQueue

**See**: [default](#module_abstract-message-queue--module.exports.default)  

* [abstract-message-queue](#module_abstract-message-queue)
    * [module.exports](#exp_module_abstract-message-queue--module.exports) ⏏
        * [new module.exports(options)](#new_module_abstract-message-queue--module.exports_new)
        * _instance_
            * [.delete()](#module_abstract-message-queue--module.exports+delete)
        * _static_
            * [.default](#module_abstract-message-queue--module.exports.default)

<a name="exp_module_abstract-message-queue--module.exports"></a>

### module.exports ⏏
**Kind**: Exported class  
<a name="new_module_abstract-message-queue--module.exports_new"></a>

#### new module.exports(options)
Construct an instance of the message queue


| Param | Type | Description |
| --- | --- | --- |
| options | <code>Object</code> | Protected methods for the queue |
| options.next | <code>function</code> | Function that gets the next message from the queue. Its return value must be an object with a "message" property and an optional "id" |
| options.retry | <code>function</code> | Function that re-queues the last item returned by `next`. The item ID will be passed as the first argument. |
| options.delete | <code>function</code> | Function that deletes the last item rturned by `next`. The item ID will be passed as the first argument. |

<a name="module_abstract-message-queue--module.exports+delete"></a>

#### module.exports.delete()
Deletes the last message from the queue

**Kind**: instance method of [<code>module.exports</code>](#exp_module_abstract-message-queue--module.exports)  
<a name="module_abstract-message-queue--module.exports.default"></a>

#### module.exports.default
Utility class to help implement the AbstractMessageQueue

**Kind**: static property of [<code>module.exports</code>](#exp_module_abstract-message-queue--module.exports)  
