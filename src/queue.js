/**
 * MessageQueue
 * @module abstract-message-queue
 * @see {@link module:abstract-message-queue.default}
 */

import { strict as assert } from 'assert';

/**
 * Utility class to help implement the AbstractMessageQueue
 * @name module:abstract-message-queue.default
 */
export default class MessageQueue {
	#options
	#hasMessage = false;
	#delete = false;

	/**
	 * Construct an instance of the message queue
	 * @param {Object} options Protected methods for the queue
	 * @param {Function} options.next Function that gets the next message from the queue. Its return value must be an object with a "message" property and an optional "id"
	 * @param {Function} options.retry Function that re-queues the last item returned by `next`. The item ID will be passed as the first argument.
	 * @param {Function} options.delete Function that deletes the last item rturned by `next`. The item ID will be passed as the first argument.
	 */
	constructor(options){
		this.#options = options;
	}

	/**
	 * Iterate over the queue
	 * @yields The next message
	 */
	async *[Symbol.asyncIterator](){
		const { next, retry, delete: del } = this.#options;
		let nextMessage;
		while( (nextMessage = await next()) ){
			const { id, message } = nextMessage;
			this.#hasMessage = true;

			try {
				yield message;
			} finally {
				if(!this.#delete) {
					retry && await retry(id);
				} else {
					del && await del(id);
				}
				this.#hasMessage = false;
				this.#delete = false;
			}
		}
	}

	/**
	 * Deletes the last message from the queue
	 */
	delete() {
		assert.notEqual(this.#hasMessage, false, 'There is no message to delete.');

		this.#delete = true;
	}
}
