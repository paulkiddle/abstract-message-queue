import { strict as assert } from 'assert';

class Timeout extends Error {}

function timeout(t) {
	return new Promise((resolve, reject) => {
		setTimeout(()=>{
			reject(new Timeout('The request timed out'));
		}, t);
	});
}

async function testNextMessage(next, t, message){
	// Get the next message
	const { value, done } = await Promise.race([ next, timeout(t) ]);

	assert(!done, 'The iterator finished early');

	// Assert the message is the one posted
	assert.deepEqual(value, message);
}

export async function* testIterator(queue, t=500) {
	const message = { body: 'I am the message' };

	await queue.send(message);

	// Create the iterator for the queue
	let iter = queue[Symbol.asyncIterator]();

	// Get the first message
	await testNextMessage(iter.next(), t, message);

	let next = iter.next();

	// Message should be requeued - allow time to return to the queue
	yield 'Allow the requeued message time to return to the front of the queue.';

	// Get the next message
	await testNextMessage(next, t, message);

	// Mark message for deletion
	queue.delete();

	next = iter.next();

	// Message should be deleted - allow time to return to the queue
	yield 'Allow the deleted message time to return to the front of the queue in case the deletion failed.';

	// There should be no more messages - check for timeout
	await assert.rejects(Promise.race([ next, timeout(t) ]), Timeout, 'The message was not deleted.');
}

/**
 * Function to test the message queue
 * @param {MessageQueue} queue Instance of a message queue to test
 * @param {function} waiter A function that waits for any requeued messages to return to the front of the queue
 * @throws {Error} If the tests fail
 */
export default async function(queue, waiter) {
	let i=0;

	for await(const message of testIterator(queue)) {
		await waiter(i++, message);
	}
}
