import queueTest from './queue-test.js';
import Queue from './queue.js';
import { jest } from '@jest/globals';

test('Test memory queue', async ()=>{
	const q = [];
	let ix = 0;

	const cfg = {
		next: jest.fn(()=>{
			if(ix >= q.length) {
				return new Promise(()=>{});
			}
			return { message: q[ix], id: ix };
		}),
		retry: jest.fn(id=>{ q.push(q[id]); ix++;}),
		delete: jest.fn(()=>{ix++;})
	};

	const queue = new Queue(cfg);
	queue.send = jest.fn(message => q.push(message)),

	expect(test).toBeInstanceOf(Function);

	await expect(queueTest(queue, () => ({}))).resolves.not.toThrowError();

	expect(cfg.retry).toHaveBeenCalledWith(0);
	expect(cfg.delete).toHaveBeenCalledWith(1);
});
